# Specify base image
FROM ruby:2.7.2

# Define work directory inside container 
WORKDIR /app

# Copy files from project to the container
COPY . .

# Install RubyOnRails dependencies
RUN bundle install

# Expose port 3000 to container
EXPOSE 3000

# Specify the command to initiate RubyOnRails server
CMD ["rails", "server", "-b", "0.0.0.0"]
